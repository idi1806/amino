# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 autotools

DESCRIPTION="simple program to reset all usb busses and devices"
HOMEPAGE="https://gitlab.com/idi1806/resetusb"
EGIT_REPO_URI="https://gitlab.com/idi1806/resetusb.git"


LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/libusb-compat"
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
	eapply_user
}
