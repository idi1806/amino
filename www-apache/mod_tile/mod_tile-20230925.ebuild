# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils autotools git-r3 apache-module


DESCRIPTION="A Apache module to efficiently render and serve tiles for OSM using mapnik"
HOMEPAGE="http://wiki.openstreetmap.org/wiki/Mod_tile"
EGIT_REPO_URI="https://github.com/openstreetmap/mod_tile.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
IUSE=""

DEPEND="
	net-misc/curl
	x11-libs/cairo
	app-arch/bzip2
	media-libs/freetype
	sci-geosciences/mapnik
	sys-libs/zlib
	dev-libs/iniparser:0
"
RDEPEND="${DEPEND}"

APACHE2_MOD_CONF="14_${PN}"
APACHE2_MOD_DEFINE="MOD_TILE"
DOCFILES="readme.txt"
#APXS2_S="${S}/apache2/src"
#APACHE2_MOD_FILE="${APXS2_S}/mod_tile.so"

need_apache2

src_prepare() {
	eautoreconf
	eapply_user
}

src_configure() {
	econf --with-apxs=${APXS}
}

src_compile() {
	make || die "make failed"
}


src_install() {

	make DESTDIR="${D}" install || die "make install failed"
	make DESTDIR="${D}" install-mod_tile

	dodoc README*
#	dodoc -r examples
	doman docs/man/*.1

	newinitd "${FILESDIR}"/renderd.rc renderd
}
