# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

KFMIN=5.74.0
QTMIN=5.15.1

inherit flag-o-matic git-r3

DESCRIPTION="Simplicity - Simple SDDM theme"
KEYWORDS="~amd64 ~arm ~arm64 ~x86"
HOMEPAGE="https://gitlab.com/isseigx/simplicity-sddm-theme"
EGIT_REPO_URI="https://gitlab.com/idi1806/simplicity-sddm-theme.git"
SLOT=0

LICENSE="GPL-3"
COMMON_DEPEND="
	>=dev-qt/qtquickcontrols2-${QTMIN}:5
	x11-misc/sddm
"
DEPEND="${COMMON_DEPEND}
"
RDEPEND="${COMMON_DEPEND}
"

# with git-r3 no longer necessary
#S="${WORKDIR}/simplicity"


src_install() {
		# sddm theme
		cd simplicity
		insinto /usr/share/sddm/themes
		doins -r "${S}/simplicity"
}
