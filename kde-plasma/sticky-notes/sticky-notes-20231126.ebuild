# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

KFMIN=5.111.0
QTMIN=5.15.1
PLMIN=5.27

inherit flag-o-matic git-r3 cmake


DESCRIPTION="Sticky Notes - QML plasmoid for Plasma 5 Desktop"
HOMEPAGE="https://github.com/m-pilia/sticky-notes"
EGIT_REPO_URI="https://github.com/m-pilia/sticky-notes.git"

LICENSE="GPL-3"
SLOT="5"
KEYWORDS="~amd64 ~x86"

DEPEND="
	>=dev-qt/qtquickcontrols-${QTMIN}:5
	>=dev-qt/qtquickcontrols2-${QTMIN}:5
	>=kde-plasma/kdeplasma-addons-${PLMIN}:5
	>=kde-frameworks/kwidgetsaddons-${KFMIN}:5
"
#>=kde-frameworks/plasma-${KFMIN}:5
#	"
RDEPEND="${DEPEND}"
BDEPEND=""


