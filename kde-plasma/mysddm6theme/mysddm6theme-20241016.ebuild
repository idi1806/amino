# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

KFMIN=6.7.0
QTMIN=6.7.3

inherit flag-o-matic git-r3


DESCRIPTION="SDDM Theme For Plasma 6 (20241017)"
HOMEPAGE="https://gitlab.com/idi1806/mysddm6theme"
EGIT_REPO_URI="https://gitlab.com/idi1806/mysddm6theme.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="
	x11-misc/sddm
"
DEPEND="${COMMON_DEPEND}
"
RDEPEND="${COMMON_DEPEND}
"

src_install() {
	# sddm qt6 theme

	dodoc README.md LICENSE
#	dodoc -r Notes

	dodir   /usr/share/sddm/themes/mysddm6theme
	insinto /usr/share/sddm/themes/mysddm6theme
	doins *.qml
	doins *.desktop
	doins *.png
	doins *.conf
	doins *.user

	doins -r faces
	doins -r assets
}

