# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

KFMIN=5.74.0
QTMIN=5.15.1

inherit flag-o-matic git-r3


DESCRIPTION="Breeze Slider J Theme for SDDM"
HOMEPAGE="https://store.kde.org/p/1174674/"
EGIT_REPO_URI="https://gitlab.com/idi1806/breeze-slider-j.git"

LICENSE="CC-BY-SA"
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="
	>=dev-qt/qtquickcontrols2-${QTMIN}:5
	x11-misc/sddm
"
DEPEND="${COMMON_DEPEND}
"
RDEPEND="${COMMON_DEPEND}
"


src_install() {
	# sddm theme

	dodoc README.md CHANGELOG LICENSE
	dodoc -r Notes

	dodir   /usr/share/sddm/themes/breeze-slider-j
	insinto /usr/share/sddm/themes/breeze-slider-j
	doins *.qml
	doins -r components
	doins *.desktop
	doins *.png
	doins *.conf

	dodir /usr/share/sddm/themes/breeze-slider-j/SlideFiles
}

