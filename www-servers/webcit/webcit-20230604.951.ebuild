# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7
inherit autotools git-r3

EGIT_REPO_URI="https://code.citadel.org/citadel/citadel.git"
EGIT_BRANCH="master"

if [[ ${PV} == "20230604.951" ]] ; then
    EGIT_BRANCH="master"
        EGIT_COMMIT="29ae62802a22ca01e54ed5bce4e7da54eefb74e0"
        SRC_URI=""
fi

DESCRIPTION="Groupdav compliant AJAX'ed Blog/Forum/Wiki/Webserver for Citadel groupware"
HOMEPAGE="http://www.citadel.org/"
# SRC_URI="http://easyinstall.citadel.org/${P}.tar.gz"
# S="${WORKDIR}/${PN}"

S="${WORKDIR}/webcit-${PV}/webcit"


LICENSE="GPL-2 MIT LGPL-3 LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ssl"


DEPEND="
	acct-group/webcit
	acct-user/webcit
	>=dev-libs/libical-0.43
	>=dev-libs/libcitadel-${PV}
	ssl? ( dev-libs/openssl )
	sys-libs/zlib"
RDEPEND="${DEPEND}"

WWWDIR="/usr/share/citadel-webcit"

#pkg_setup() {
#	#Homedir needs to be the same as --with-datadir
#	einfo "Adding Citadel User/Group"
#	enewgroup webcit
#	enewuser webcit -1 -1 ${WWWDIR} webcit
#}

src_prepare() {
	eautoreconf
	eapply_user
}

src_configure() {
	./configure CFLAGS="-I/usr/local/ctdlsupport/include" LDFLAGS="-L/usr/local/ctdlsupport/lib64" CPPFLAGS="-I/usr/local/ctdlsupport/include"
	scripts/mk_module_init.sh

#	econf \
#		$(use_with ssl) \
#		--prefix=/usr/sbin/ \
#		--with-datadir=/var/run/citadel \
#		--with-editordir=/usr/share/citadel-webcit/tiny_mce/ \
#		--with-localedir=/usr/share/ \
#		--with-rundir=/var/run/citadel \
#		--with-ssldir=/etc/ssl/webcit/ \
#		--with-wwwdir="${WWWDIR}"
}

src_install() {
	emake DESTDIR="${D}" install
	newinitd "${FILESDIR}"/webcit.init.d webcit-ssl
	newinitd "${FILESDIR}"/webcit.init.d webcit

	newconfd "${FILESDIR}"/webcit.conf.d webcit-ssl
	newconfd "${FILESDIR}"/webcit.conf.d webcit


	##House cleaning...
	#We don't use the setup program, settings are in /etc/conf.d/webcit
	# 2022: no longer exists
	#rm "${D}"/usr/sbin/setup || die "Removing upstreams setup bin failed"

	dodoc *.txt
}

pkg_postinst() {
	elog "You can now connect more than one Citadel server with different configs:"
	elog "Make sure to configure webcit under /etc/conf.d/webcit(.yourinstance)."
	elog "Then start the server with /etc/init.d/webcit(.yourinstance) start"
	elog
	elog "Webcit will listen on port 8087"
}
