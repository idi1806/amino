# Amino

My personal Gentoo Overlay for some ebuild scripts.
* for KDE/QT (liquidshell, qcad, etc),
* for administration (finding cruft files. etc),
* for network, crypto (steghide, aca_sig)

This overlay tries also to add some additional authenticity checks (when authors of the original software provide signed sources)


## Adding the overlay

### Official list

    layman -a amino

or

    eselect repository enable amino


### From URL
    layman -o 'https://gitlab.com/idi1806/amino/-/raw/master/amino.xml' -f -a amino

### Manual
Create /etc/portage/repos.conf/amino.conf

    [amino]
    location = /var/db/repos/amino
    sync-uri = https://gitlab.com/idi1806/amino.git
    sync-type = git
    auto-sync = yes

