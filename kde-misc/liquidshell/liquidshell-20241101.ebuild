# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

KFMIN=6.5.0
QTMIN=6.7.2

VIRTUALX_REQUIRED="test"
inherit git-r3 cmake
# ecm kde.org

DESCRIPTION="liquidshell, the plamashell replacement "
HOMEPAGE="https://github.com/KDE/liquidshell"
EGIT_REPO_URI="https://github.com/KDE/liquidshell.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="mini"

DEPEND="
	>=dev-qt/qtbase-${QTMIN}:6[dbus,gui,network,widgets]
	>=kde-frameworks/karchive-${KFMIN}:6
	>=kde-frameworks/kcmutils-${KFMIN}:6
	>=kde-frameworks/kconfig-${KFMIN}:6
	>=kde-frameworks/kconfigwidgets-${KFMIN}:6
	>=kde-frameworks/kcoreaddons-${KFMIN}:6
	>=kde-frameworks/kcrash-${KFMIN}:6
	>=kde-frameworks/kdbusaddons-${KFMIN}:6
	>=kde-frameworks/ki18n-${KFMIN}:6
	>=kde-frameworks/kiconthemes-${KFMIN}:6
	>=kde-frameworks/kio-${KFMIN}:6
	>=kde-frameworks/kitemviews-${KFMIN}:6
	>=kde-frameworks/knewstuff-${KFMIN}:6
	>=kde-frameworks/knotifications-${KFMIN}:6
	>=kde-frameworks/kservice-${KFMIN}:6
	>=kde-frameworks/kwidgetsaddons-${KFMIN}:6
	>=kde-frameworks/kwindowsystem-${KFMIN}:6
	>=kde-frameworks/kxmlgui-${KFMIN}:6
	>=kde-frameworks/solid-${KFMIN}:6
"

#   >=kde-frameworks/bluez-qt-${KFMIN}:6
#>=kde-frameworks/networkmanager-qt-${KFMIN}:6

#	>=dev-qt/qtwidgets-${QTMIN}:5
#	>=kde-frameworks/kcmutils-${KFMIN}:5
#	>=kde-frameworks/networkmanager-qt-${KFMIN}:5
#	>=kde-frameworks/bluez-qt-${KFMIN}:5
#	>=kde-frameworks/knewstuff-${KFMIN}:5
#	dev-libs/appstream"
#
RDEPEND="${DEPEND}
	>=dev-qt/qttools-${QTMIN}:6[qdbus]
	>=kde-frameworks/kded-${KFMIN}:6
	sys-apps/dbus
	x11-themes/hicolor-icon-theme
"


BDEPEND=""

#............................................................

src_prepare() {
	if use mini; then
		einfo "no packagekit stuff"
		eapply "${FILESDIR}"/nopackagekit6.patch
		einfo "no bluetooth, no network manager"
		eapply "${FILESDIR}"/no_nm_no_blue6.patch
	fi
	eapply_user
	cmake_src_prepare
}

#src_configure() {
#	local mycmakeargs=(
#		-DCMAKE_DISABLE_FIND_PACKAGE_packagekitqt6=ON
#	)
#	ecm_src_configure
#}


src_compile() {

	einfo "make liquidshell"

#	emake
	rm -rf build
	mkdir -p build
	cd build
	cmake ..
	emake
}

#............................................................
src_install() {
	cd build

	emake DESTDIR="${D}" install

}

