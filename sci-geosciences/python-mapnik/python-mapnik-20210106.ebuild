# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{8,9,10,11} )
inherit distutils-r1 git-r3

DESCRIPTION="Python bindings for the mapnik"
HOMEPAGE="https://github.com/mapnik/python-mapnik"
EGIT_REPO_URI="https://github.com/mapnik/python-mapnik"
EGIT_SUBMODULES=( '*' '-test-*' )
EGIT_BRANCH="v3.0.x"
EGIT_COMMIT="7da019c"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	dev-libs/boost:=[python,${PYTHON_USEDEP}]
	<sci-geosciences/mapnik-3.1
	${PYTHON_DEPS}
"
DEPEND="${RDEPEND}"

DOCS=( AUTHORS.md CHANGELOG.md CONTRIBUTING.md README.md )
PATCHES=(
        "${FILESDIR}/flags_fix.patch"
        "${FILESDIR}/paths_fix.patch"
		"${FILESDIR}/20210105.patch"
)


#src_unpack() {
#	git-r3_fetch ${EGIT_REPO_URI} ${REFS} ${TAG}
#	git-r3_checkout ${EGIT_REPO_URI} ${WORKDIR}/${P} ${TAG}
#}


python_compile() {
	my_ver=${EPYTHON/python/}

	BOOST_PYTHON_LIB=boost_python-${my_ver} distutils-r1_python_compile
}

python_install() {
	BOOST_PYTHON_LIB=boost_python-${my_ver} distutils-r1_python_install
}
