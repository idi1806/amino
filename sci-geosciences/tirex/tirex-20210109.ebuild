# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils git-r3 user

DESCRIPTION="Tirex OSM renderer and tile server."
HOMEPAGE="http://wiki.openstreetmap.org/index.php/Tirex"
#EGIT_REPO_URI="https://github.com/stoecker/tirex.git"
EGIT_REPO_URI="https://github.com/openstreetmap/tirex.git"



LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
DEPEND="
	acct-group/tirex
	acct-user/tirex
	dev-perl/GD
	dev-perl/JSON
	dev-perl/IPC-ShareLite
	dev-perl/libwww-perl
	virtual/perl-Test-Harness
	virtual/perl-Test-Simple
	sci-geosciences/mapnik"
RDEPEND="${DEPEND}"

IUSE=""



#src_compile() {
#}

src_install() {
	emake DESTDIR="${D}" install
	dodoc LICENSE README BUGS
	dodir /var/log/tirex
	dodir /var/lib/tirex
	dodir /var/lib/tirex/tiles
	dodir /var/lib/tirex/stats
	chown -Rh tirex:tirex ${ED}/var/lib/tirex
	chown -Rh tirex:tirex ${ED}/var/log/tirex

	newinitd "${FILESDIR}"/tirex-backend-manager.rc tirex-backend-manager
	newinitd "${FILESDIR}"/tirex-master.rc          tirex-master
}

#pkg_postinst() {
#}
