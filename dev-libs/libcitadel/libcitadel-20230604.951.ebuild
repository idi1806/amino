# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

inherit autotools flag-o-matic git-r3

EGIT_REPO_URI="https://code.citadel.org/citadel/citadel.git"
EGIT_BRANCH="master"

if [[ ${PV} == "20230604.951" ]] ; then
    EGIT_BRANCH="master"
        EGIT_COMMIT="29ae62802a22ca01e54ed5bce4e7da54eefb74e0"
        SRC_URI=""
fi

# XMY_LIBV="3"
# XS="${WORKDIR}/${PN}-${MY_LIBV}.${PV}"

DESCRIPTION="Code shared across all the components of a Citadel system"
HOMEPAGE="http://citadel.org/"
# SRC_URI="http://easyinstall.citadel.org/${P}.tar.gz"
# S="${WORKDIR}/${PN}"

S="${WORKDIR}/libcitadel-${PV}/libcitadel"


LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	sys-libs/libxcrypt
	dev-libs/expat
	dev-libs/libical
	net-misc/curl
	mail-filter/libsieve
	sys-libs/db
	sys-libs/zlib"
RDEPEND="${DEPEND}"

src_prepare() {
	# eaclocal
	# autoupdate
	eautoconf
	autoheader
	eapply_user
}

src_configure() {
	filter-flags -finline-functions
	replace-flags -O3 -O2

	econf \
		--prefix=/usr/local/ctdlsupport \

	# don't like a ldconfig in the Makefile
	sed -e "s/ldconfig || true//g" -i Makefile


#
}

src_install() {
	insinto /etc/ld.so.conf.d/
	newins "${FILESDIR}/99-libcitadel-x86_64.conf" 99-libcitadel-x86_64.conf

	emake DESTDIR="${D}" install
}
