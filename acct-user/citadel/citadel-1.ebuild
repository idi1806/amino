# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="User for Citadel"
ACCT_USER_ID=623
ACCT_USER_GROUPS=( citadel mail  )

ACCT_USER_HOME=/usr/local/citadel
# ACCT_USER_HOME_PERMS=770


acct-user_add_deps

