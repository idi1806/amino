# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="user for tirex"
ACCT_USER_ID=277
ACCT_USER_GROUPS=( tirex )
ACCT_USER_HOME=/var/lib/tirex
ACCT_USER_HOME_PERMS=0700

acct-user_add_deps

