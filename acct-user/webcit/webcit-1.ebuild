# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="User for Citadel Web"
ACCT_USER_ID=624
ACCT_USER_GROUPS=( webcit )

ACCT_USER_HOME="/usr/local/webcit"
#ACCT_USER_HOME_PERMS=770


acct-user_add_deps

