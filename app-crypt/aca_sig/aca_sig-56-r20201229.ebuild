# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools amino desktop

MY_P="aca_sig-b56"
MY_PTGZ="${MY_P}.tar.gz"
DESCRIPTION="elliptic curve cryptography"
HOMEPAGE="https://www.academic-signature.org"
SRC_URI="https://www.academic-signature.org/accessories/aca_sig_sout.php -> ${MY_PTGZ}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+gpgverify"

DEPEND="
	>=x11-libs/wxGTK-3.0.4
	>=app-crypt/gnupg-2.2.20"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	if use gpgverify; then
        einfo "Trying to verify TAR file/signature:"
        einfo "- Can only be successful if root/portage's gpg knows the public key of Michael Anders"
        einfo "- public key can also be downloaded from ${HOMEPAGE}"
        amino_verify_src_manders \
			"${FILESDIR}"/${MY_PTGZ}.sig \
			"${DISTDIR}"/${MY_PTGZ}
	fi
	unpack ${A}
	cp -r ${WORKDIR}/${MY_P} ${S} || die
	cd ${S}
	einfo "patch in ${S}"

	# embed icon and bitmap as xpms in the application
	# (avoids loading of these files from somewhere)
	eapply "${FILESDIR}"/aca_sig-b56.embedded-xpm.patch
	eapply "${FILESDIR}"/aca_sig-b56.embedded-xpm2.patch
	eapply "${FILESDIR}"/aca_sig-b56.embedded-xpm3.patch

	# fixes parts of the problems when starting aca_sig the first time
	# (when $HOME/.config/aca_sig not exists and must be created)
	# still results in a "Floating point exception"
	eapply "${FILESDIR}"/aca_sig-b56.config_v2.patch

	# Adds a warning text to the last note before the "Floating point
	# exception occurs" - this is only an intermediate / workaround
	# patch !!
	eapply "${FILESDIR}"/aca_sig-b56.secs-workaround.patch
}

src_prepare() {
	# NOTE: Commands are from bootstrap.sh.
	eaclocal
	eautoheader
	eautomake --add-missing
	eautoconf
	default
}


src_install() {
	emake DESTDIR="${D}" install

	doicon --size 32 aca_sig_icon2.png
	doicon --size 64 aca_sig_icon.png

	dodir   /var/lib/aca_sig
	insinto /var/lib/aca_sig
	doins -r virgin_secrets

	einstalldocs
	dodoc README ChangeLog
	dodoc -r virgin_secrets/.config/aca_sig/key_tray
	dodoc -r virgin_secrets/.config/aca_sig/x_secrets
	dodoc "${FILESDIR}"/aca_sig_create_key.pdf
	dodoc "${FILESDIR}"/aca_sig_shots01.pdf

	docompress -x /usr/share/doc/${PF}/key_tray
	docompress -x /usr/share/doc/${PF}/x_secrets

	einfo "basic key stuff is also installed in doc"
	einfo "  /usr/share/doc/${PF}/x_secrets"
	einfo "  /usr/share/doc/${PF}/key_tray"

}
