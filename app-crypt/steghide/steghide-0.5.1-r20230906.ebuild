# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v

EAPI="8"
inherit autotools

DESCRIPTION="A steganography program which hides data in various media files"
HOMEPAGE="http://steghide.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

DEPEND=">=app-crypt/mhash-0.8.18-r1
	>=dev-libs/libmcrypt-2.5.7
	>=sys-libs/zlib-1.1.4-r2
	virtual/jpeg"
RDEPEND="${DEPEND}"

src_prepare(){
	eapply "${FILESDIR}"/${P}-gcc41.patch
	eapply "${FILESDIR}"/${P}-mhash.patch
	eapply "${FILESDIR}"/${P}-gcc43.patch
	eapply "${FILESDIR}"/${P}-gcc73.patch
	eautoreconf
	eapply_user
}

src_configure() {
	econf $(use_enable debug)
}

src_compile() {
	local libtool
	[[ ${CHOST} == *-darwin* ]] && libtool=$(type -P glibtool) || libtool=$(type -P libtool)
	emake LIBTOOL="${libtool}" || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" docdir="${EPREFIX}/usr/share/doc/${PF}" install || die "emake install failed"
	#	prepalldocs
}
