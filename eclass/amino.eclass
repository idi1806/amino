# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: amino.eclass
# @MAINTAINER: J.Schlick
# idi1806 <gitty@raptorforce.de>
# @AUTHOR: J.Schlick
# idi1806 <gitty@raptorforce.de>
# @BLURB:
# @DESCRIPTION:


# Workaround for verifying git tags
# Feature request: https://bugs.gentoo.org/733430
amino_verify_sources_git() {
	AMINO_OVERLAY_DIR="$(portageq get_repo_path / amino)"
	# Import amino developers keys
	gpg --import "${AMINO_OVERLAY_DIR}/keys/Amino_20201227_20290315.asc" 2>/dev/null
	# Trust amino Master Signing Key
	echo '2B9631E65F3C775B80BBA20BD836D9A00A925519:6:' | gpg --import-ownertrust

	VALID_TAG_FOUND=0
	for tag in $(git tag --points-at="$1"); do
		if git verify-tag --raw "$tag" 2>&1 | grep -q '^\[GNUPG:\] TRUST_\(FULLY\|ULTIMATE\)'; then
			VALID_TAG_FOUND=1
		fi
	done

	if [ "$VALID_TAG_FOUND" -eq 0 ]; then
		die 'Signature verification failed!'
	fi
}


# verifying downloaded sources:with Amino key...
#      $1 - file with the detached signature
#      $2 - file which is signed
amino_verify_src_amino() {
		AMINO_OVERLAY_DIR="$(portageq get_repo_path / amino)"
		# Import amino developers keys
		gpg --import "${AMINO_OVERLAY_DIR}/keys/Amino_20201227_20290315.asc" 2>/dev/null
		# Trust amino Amino Signing Key
		echo '2B9631E65F3C775B80BBA20BD836D9A00A925519:6:' | gpg --import-ownertrust

		VALID_SIG_FOUND=0
		# see the output of gpg in the log - should be commented out
		gpg --verify $1 $2 2>&1
		if gpg --verify $1 $2 2>&1 | grep -q '^gpg: Good signature from '; then
			VALID_SIG_FOUND=1
		fi

		if [ "$VALID_SIG_FOUND" -eq 0 ]; then
				die "Signature verification for source file $2 failed!"
		fi
}


# verifying downloaded sources:with MAnders key...
#      $1 - file with the detached signature
#      $2 - file which is signed
amino_verify_src_manders() {
		AMINO_OVERLAY_DIR="$(portageq get_repo_path / amino)"
		# Import amino developers keys
		gpg --import "${AMINO_OVERLAY_DIR}/keys/MAnders_20120105_20220102.asc" 2>/dev/null
		# Trust amino MAnders Signing Key
		echo '78A0129054ED52DF72F0E86D70C5D7741C6D685A:6:' | gpg --import-ownertrust

		VALID_SIG_FOUND=0
		# see the output of gpg in the log - should be commented out
		gpg --verify $1 $2 2>&1
		if gpg --verify $1 $2 2>&1 | grep -q '^gpg: Good signature from '; then
			VALID_SIG_FOUND=1
		fi

		if [ "$VALID_SIG_FOUND" -eq 0 ]; then
				die "Signature verification for source file $2 failed!"
		fi
}
